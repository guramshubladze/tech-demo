# Techub Demo project
### This repo is for techub demo


## how to get github account

First, you must make a user account on Gitlab if you don't have one already. Be sure to choose a user ID that you are happy using for the rest of your professional career as a developer. GitHub is a very important tool, you will be using this account again in the future. Join gitlab here:
[gitlab.com](https://gitlab.com/users/sign_in#register-pane).



## Bold and italik in README

*This text will be italic*


**This text will be bold**



## This is unordered list 

* Item 1
* Item 2
  * Item 2a
  * Item 2b

## Javascript code

```javascript
function fancyAlert(arg) {
  if(arg) {
    $.facebox({div:'#foo'})
  }
}
```
## Python Code

```python
def foo():
    if not bar:
        return True
```


### Fork the Repository

Now that you have your own GitHub account you can fork [this](https://github.com/EEOB-BioData/Unix-Git-Exercise) repository.
A _fork_ creates a copy of a GitHub repository in your own GitHub account. Thus, you have permission to make changes to the content of your copied repository without ever changing the original one. At the time of your fork, you will copy all of the current contents and the two repositories are now independent. So if someone commits changes to the original repository, they will not affect your forked copy.

Fork the original repository [https://github.com/EEOB-BioData/Unix-Git-Exercise](https://github.com/EEOB-BioData/Unix-Git-Exercise) by clicking the **_Fork_** button at the upper right corner of the repository page.

<img src="https://miro.medium.com/max/5772/1*FtYgYG_G6rplUmF5fLzuXA.png">

This will take you to the GitHub page for your very own GitHub repository! It should have a URL like the one below (where `<your GitHub ID>` should be your new GitHub ID):

```
https://github.com/<your GitHub ID>/Unix-Git-Exercise
```

![Image of Yaktocat](https://octodex.github.com/images/yaktocat.png)

